﻿// GAME 221 Lab 03 2D Sprite Creation Tool 
//  This script is a recreation of the provided lab example.
// @Linda Lane
// @Sept. 7, 2016
// @Class Create2DSpriteSheet 


using UnityEngine;
using System.Collections;
using UnityEditor;

public class Create2DSpriteSheet : EditorWindow {

    public static Object selectedObject;

    int numAnimations;
    string controllerName;
    string[] animationNames = new string[100];
    float[] clipFrameRate = new float[100];
    float[] clipTimeBetween = new float[100];
    int[] startFrames = new int[100];
    int[] endFrames = new int[100];
    bool[] pingPong = new bool[100];
    bool[] loop = new bool[100];

    // Set up menu item for 2D Animations
        [MenuItem("Project Tools/2D Animations")]

    // Function Init is used to 
    // 1. store the object that was selected in the hierarchy
    // 2. open a popup window

        
    public static void Init()
    {
        selectedObject = Selection.activeObject;    // store the selected object.
        if (selectedObject == null)                 // check to see if the selected object exists; if it doesn't return.
            return;
        Create2DSpriteSheet window = (Create2DSpriteSheet)EditorWindow.GetWindow(typeof(Create2DSpriteSheet));
        window.Show();

    }

    void OnGUI()
    {
        EditorGUILayout.LabelField("Animations for " + selectedObject.name);  // Label the name of the GO the sprites will be pulled from
        EditorGUILayout.Separator();     // Display a space using Separator
        controllerName = EditorGUILayout.TextField("Controller Name", controllerName);  // Make an input box for user to enter the controller name
        numAnimations = EditorGUILayout.IntField("How many animations?", numAnimations); // ask how many animations this program will create (use an int field for number inputs)

        // loop through the array of animations to be created, filling in information
        // Loop through each theoretical animation
        for (int i = 0; i < numAnimations; i++)
        {
            //Determine a name for the animation
            animationNames[i] = EditorGUILayout.TextField("Animation Name", animationNames[i]);

            //Start a section where the following items will be displayed horizontally instead of vertically
            EditorGUILayout.BeginHorizontal();

            //Determine the start frame for the animation
            startFrames[i] = EditorGUILayout.IntField("Start Frame", startFrames[i]);

            //Determine the end frame for the animation
            endFrames[i] = EditorGUILayout.IntField("End Frame", endFrames[i]);

            //End the section where the previous items are displayed horitontally instead of vertically
            EditorGUILayout.EndHorizontal();

            //Start a section where the following items will be displayed horizontally instead of vertically
            EditorGUILayout.BeginHorizontal();

            //Determine the frame rate for the animation
            clipFrameRate[i] = EditorGUILayout.FloatField("Frame Rate", clipFrameRate[i]);

            //Determine the space between each keyframe
            clipTimeBetween[i] = EditorGUILayout.FloatField("Frame Spacing", clipTimeBetween[i]);

            //End the section where the previous items are displayed horitontally instead of vertically
            EditorGUILayout.EndHorizontal();

            //Start a section where the following items will be displayed horizontally instead of vertically
            EditorGUILayout.BeginHorizontal();

            //Create a checkbox to determine if this animation should loop
            loop[i] = EditorGUILayout.Toggle("Loop", loop[i]);

            //Create a checkbox to determine if this animation should pingpong
            pingPong[i] = EditorGUILayout.Toggle("Ping Pong", pingPong[i]);

            //End the section where the previous items are displayed horitontally instead of vertically
            EditorGUILayout.EndHorizontal();

            //Create a space
            EditorGUILayout.Separator();
        }

        // Create a button with the label "Create" to create these animations automatically
        if (GUILayout.Button("Create"))
        {
            // If teh button has been pressed, create the controller
            UnityEditor.Animations.AnimatorController controller = UnityEditor.Animations.AnimatorController.CreateAnimatorControllerAtPath(("Assets/" + controllerName + ".controller"));

            for (int i = 0; i < numAnimations; i++)
            {
                //Create each animation clip
                AnimationClip tempClip = CreateClip(selectedObject, animationNames[i], startFrames[i], endFrames[i], clipFrameRate[i], clipTimeBetween[i],  pingPong[i]);

                if (loop[i])
                {
                    //set the Loop on the clip to true
                    // extract the animation clip's settings
                    // edit the settings
                    // reassign the modified settings back onto the clip
                    AnimationClipSettings settings = AnimationUtility.GetAnimationClipSettings(tempClip);
                    settings.loopTime = true;
                    settings.loopBlend = true;
                    AnimationUtility.SetAnimationClipSettings(tempClip, settings);

                }

                controller.AddMotion(tempClip);
            }
        }
    }

    // Function for creating the animation, with parameters supplied from editor input

    public AnimationClip CreateClip(Object obj, string clipName, int startFrame, int endFrame, float frameRate, float timeBetween, bool pingPong)
    {
        // Get the path to the object that contains all the sprites
        string path = AssetDatabase.GetAssetPath(obj);

        // Load (extract) the sprites from the object
        Object[] sprites = AssetDatabase.LoadAllAssetsAtPath(path);

        // Determinie how many frames therea re going to be including one extra frame, and the length of each frame
        int frameCount = endFrame - startFrame + 1;

        // Determine how long the animation is going to be
        float frameLength = 1f / timeBetween;

        // Create a new animation
        AnimationClip clip = new AnimationClip();

        // set up the frame rate
        clip.frameRate = frameRate;

        // Curve Binding
        // Bind the clip as a Sprite Renderer animation
        EditorCurveBinding curveBinding = new EditorCurveBinding();

        // Assign it to change the sprite renderer
        curveBinding.type = typeof(SpriteRenderer);
        curveBinding.propertyName = "m_Sprite";

        // Set up the key frames, accounting for pingPong

        //Create a container for all of the keyframes
        ObjectReferenceKeyframe[] keyFrames;

        //Determine how many frames there will be if we are or are not pingponging
        if (!pingPong)
            keyFrames = new ObjectReferenceKeyframe[frameCount + 1];
        else
            keyFrames = new ObjectReferenceKeyframe[frameCount * 2 + 1];

        //Keep track of what frame number we are on
        int frameNumber = 0;

        //Loop from start to end, incrementing frameNumber as we go
        for (int i = startFrame; i < endFrame + 1; i++, frameNumber++)
        {
            //Create an empty keyframe
            ObjectReferenceKeyframe tempKeyFrame = new ObjectReferenceKeyframe();
            //Assign it a time to appear in the animation
            tempKeyFrame.time = frameNumber * frameLength;
            //Assign it a sprite
            tempKeyFrame.value = sprites[i];
            //Place it into the container for all the keyframes
            keyFrames[frameNumber] = tempKeyFrame;
        }

        //If we are pingponging this animation
        if (pingPong)
        {
            //Create keyframes starting at the end and going backwards
            //Continue to keep track of the frame number
            for (int i = endFrame; i >= startFrame; i--, frameNumber++)
            {
                ObjectReferenceKeyframe tempKeyFrame = new ObjectReferenceKeyframe();
                tempKeyFrame.time = frameNumber * frameLength;
                tempKeyFrame.value = sprites[i];
                keyFrames[frameNumber] = tempKeyFrame;
            }
        }

        //Create the last sprite to stop it from switching quickly from the last frame to the first one
        ObjectReferenceKeyframe lastSprite = new ObjectReferenceKeyframe();
        lastSprite.time = frameNumber * frameLength;
        lastSprite.value = sprites[startFrame];
        keyFrames[frameNumber] = lastSprite;


        //Assign the name
        clip.name = clipName;

        //Apply the curve
        AnimationUtility.SetObjectReferenceCurve(clip, curveBinding, keyFrames);

        //Creat the clip
        AssetDatabase.CreateAsset(clip, ("Assets/" + clipName + ".anim"));

        //return the clip
        return clip;
    }

    void OnFocus()
    {
        if (EditorApplication.isPlayingOrWillChangePlaymode)
            this.Close();
    }
}

