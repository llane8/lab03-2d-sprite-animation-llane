﻿// GAME 221 Lab 02 Creating Prefabs Sept 4 2016

// @Linda Lane
// @Sept. 4, 2016
// @Class CreatePrefabs adds a drop down menu item to the Unity top tool bar 
//  to generate Prefab(s) from existing materials and meshes selected in the Hierarchy View.
// If they do not exist, the script prompts the user for a location to create them, creates them,
// and deletes the objects selected in the Hierarchy and replaces them with Prefabs.

using UnityEngine;
using System.Collections;
using System.IO;
using UnityEditor;

public class CreatePrefabs : MonoBehaviour
{

    // Set up the Tool bar Menu Item
    [MenuItem("Project Tools/Create Prefab")]

    // CreatePrefab() will create Prefabs from selected game objects in the Hierarchy
    // It will protect the user from accidentally overwriting existing Prefabs.
    public static void CreatePrefab()
    {
        // set up an array of all selected game objects from heirarchy
        GameObject[] selectedObjects = Selection.gameObjects;

        // go through the array, assign each a name and its path in the top level Assets folder
        foreach (GameObject gameObj in selectedObjects)
        {
            string name = gameObj.name;
            string assetPath = "Assets/" + name + ".prefab";

            // Check to see if the game object already exists as a Prefab
            if (AssetDatabase.LoadAssetAtPath(assetPath, typeof(GameObject)))
            {
                Debug.Log("Prefab " + gameObj.name + " already exists!");

                // If prefab already exists, ask if user wants to overwrite it with a new prefab
                if (EditorUtility.DisplayDialog("Caution", "Prefab " + gameObj.name + " already exists. Do you want to overwrite it?", "Yes", "No"))
                {
                    CreateNew(gameObj, "Assets/" + name + ".prefab");
                    //Debug.Log("Overwriting" + gameObj.name + "!");
                }
            }
            else
            {
                // Debug.Log(gameObj.name + " does not exist! Creating it in Path " + "assetPath");
                // Extra Credit:  ask the user where in the Assets folder to save the file
                EditorUtility.SaveFilePanelInProject("Select Prefab", gameObj.name, "prefab", "OK");
                CreateNew(gameObj, "Assets/" + name + ".prefab");
            }
        }
        // Debug.Log("Name: " + gameObj.name + " Path: " + assetPath);
    }


    //Create the new (empty) prefab
    public static void CreateNew(GameObject obj, string location)
    {
        Debug.Log("Creating Empty Prefab at " + location);
        Object prefab = PrefabUtility.CreateEmptyPrefab(location);

        Debug.Log("Putting selected Hierarchy object into empty Prefab");
        PrefabUtility.ReplacePrefab(obj, prefab);

        Debug.Log("Refreshing database");
        AssetDatabase.Refresh();

        Debug.Log("Destroying " + obj.name);
        DestroyImmediate(obj);

        Debug.Log("Instantiating a new Prefab from the new Prefab");
        GameObject clone = PrefabUtility.InstantiatePrefab(prefab) as GameObject;
    }

}
